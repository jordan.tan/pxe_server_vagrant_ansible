========================================================
[VAGRANT][ANSIBLE] Déploiement d'un serveur PXE CentOS 7
========================================================

Ce projet permet de configurer un serveur PXE dans une VM.

OS déployés : Centos 7 (sans kickstart pour le moment).

Requirements
------------

Nécessite l'installation de Vagrant et de Ansible.

.. code:: bash

    $ sudo apt-get update
    $ sudo apt-get install vagrant
    $ sudo apt-get install ansible

Déploiement
-----------
    
Clonage du repository.

.. code:: bash

    $ git clone https://gitlab.com/jordan.tan/pxe_server_vagrant_ansible && cd pxe_server_vagrant_ansible

Création de la VM avec Vagrant puis déploiement du serveur PXE avec Ansible.

.. code:: bash

    $ sudo vagrant up

Utilisation
-----------

Il suffit maintenant de connecter les machines PXE à déployer à votre machine physique (celle qui contient la VM) pour effectuer l'installation de l'OS.
